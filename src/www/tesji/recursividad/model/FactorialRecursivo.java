package www.tesji.recursividad.model;
import java.util.Scanner;

public class FactorialRecursivo {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		FactorialRecursivo objF = new FactorialRecursivo();
		int numero;
		int tipoDeCalculo;
		
		System.out.print("\nIngresa un numero :");
		numero = entrada.nextInt();
		
		System.out.print("\n1)Recursividad\n2)Ciclo :");
		tipoDeCalculo = entrada.nextInt();
			
		System.out.print("\nEl factorial de " +numero+ " es " + objF.factorial(numero, tipoDeCalculo));		
	}
	
	public int factorial(int numero, int tipoDeCalculo) {
		int factor = 1;
		if(numero < 0) {
			return 0;
		}else {
			if (numero == 1) {////////////se calcula por recursividad
				return 1;
			}
			if (tipoDeCalculo == 1) {
				return numero * factorial(numero - 1, tipoDeCalculo);
			}else {//////////////////////se calcula mediante un ciclo.
				while (numero != 0) {
					factor = factor * numero; 
					numero--;
				}
				return factor;
			}	
		}
	}
}